from blind_watermark import WaterMark
import os

def embed_watermark(watermark_text):
    # Create a WaterMark object
    bwm1 = WaterMark(password_img=1, password_wm=1)

    # Read the image
    filename = 'test.png'
    bwm1.read_img(os.path.join('uploads', filename))

    # Define the watermark
    wm = watermark_text

    # Read the watermark
    bwm1.read_wm(wm, mode='str')

    # Embed the watermark into the image
    embedded_filename = 'embedded.png'
    bwm1.embed(os.path.join('uploads', embedded_filename))

    # Calculate the length of the watermark
    len_wm = len(bwm1.wm_bit)

    # Save the length of the watermark to a text file
    with open(os.path.join('uploads', 'len_wm.txt'), 'w') as f:
        f.write(str(len_wm))
