from blind_watermark import WaterMark
import os

def extract_watermark():
    # Create a WaterMark object
    bwm1 = WaterMark(password_img=1, password_wm=1)

    # Read the watermarked image
    len_wm = int(open(os.path.join('uploads', 'len_wm.txt'), "r").read())
    wm_extract = bwm1.extract(os.path.join('uploads', 'embedded.png'), wm_shape=len_wm, mode='str')

    return wm_extract
