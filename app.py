from flask import Flask, render_template, request, redirect, url_for, send_file
from lib import embed_watermark
from libe import extract_watermark
import os

app = Flask(__name__, template_folder=os.path.abspath(''))

UPLOAD_FOLDER = 'uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/embed', methods=['POST'])
def embed():
    watermark_text = request.form['watermark']
    file = request.files['file']
    if file.filename == '':
        return 'No selected file'
    if file and allowed_file(file.filename):
        filename = 'test.png'
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        embed_watermark(watermark_text)
        return 'Watermark embedded successfully!'
    else:
        return 'Allowed file types are png, jpg, jpeg, gif'

@app.route('/extract', methods=['POST'])
def extract():
    if request.method == 'POST':
        file = request.files['file']
        if file.filename == '':
            return 'No selected file'
        if file and allowed_file(file.filename):
            filename = 'embedded.png'
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            watermark = extract_watermark()
            return f'Extracted Watermark: {watermark}'
        else:
            return 'Allowed file types are png, jpg, jpeg, gif'
    return redirect(url_for('index'))

@app.route('/download')
def download_file():
    path = os.path.join(app.config['UPLOAD_FOLDER'], 'embedded.png')
    return send_file(path, as_attachment=True)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in {'png', 'jpg', 'jpeg', 'gif'}

if __name__ == '__main__':
    app.run(debug=True)
